<div id="content">
	<div class="row">
		<h1>About Us</h1>
		<div class="inner-about">
			<p>All Pro Pool service and Repairs is a well established family owned pool service, equipment repair and new equipment installation company. We have been servicing West Pasco, N Pinellas, and NW Hillsborough counties for the last 8 years. We have built a very reputable company in that time. Prior to residing in Pasco County we owned and maintained hotels in Panama City Beach where we were were certified to maintain our commercial pools in Bay County for 14 years. All Pro Pool Service and Repairs has an A+ rating and has received numerous awards from Home Advisor and Angie's list form year one and continue to receive them. We, at All Pro Pools pride ourselves in maintaining the best of service for our valued customers. You will find we are almost always on call for emergencies and will respond promptly. I am certain you will find our prices competitive and our service superior.</p>
		</div>
	</div>
</div>
