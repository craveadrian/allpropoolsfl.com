<div id="content">
	<div class="row">
		<h1>Testimonials</h1>
		<dl class="testibox">
			<dt> 6/6/18 Review by Deb S. in Holiday, <span class="star">&#9733; &#9733; &#9733; &#9733; &#9733;</span></dt>
			<dd><p>Project: Clean or Maintain a Swimming Pool</p>
			<p>Comments: Rick stopped by the same day I asked for an appointment and after agreeing to hire All Pro Pool Service, work was started immediately. took care of my pool cleaning and stopped by the next day to finish the initial cleaning. I have hired this company for weekly pool care. Very happy with the work and my experience with Rick.</p>
				</dd>
			</dl>

			<dl class="testibox">
				<dt> 2/6/18 Review by Maurice W. in Port Richey, <span class="star">&#9733; &#9733; &#9733; &#9733; &#9733;</span></dt>
				<dd><p>Project: Clean or Maintain a Swimming Pool</p>
				<p>Comments: they got back to me quick and cleaned my pool</p>
				</dd>
			</dl>

			<dl class="testibox">
				<dt> 7/17/16 Review by a Homeowner in in Holiday, <span class="star">&#9733; &#9733; &#9733; &#9733; &#9733;</span></dt>
				<dd><p>Project: Clean or Maintain a Swimming Pool</p>
				<p>Comments: FAST RESPONCE & QUALITY JOB FROM KNOWLEDGEABLE, FRIENDLY PEOPLE :-)))</p>
				</dd>
			</dl>

			<dl class="testibox">
				<dt> 5/26/13 Review by Judy C. in New Port Richey, <span class="star">&#9733; &#9733; &#9733; &#9733; &#9733;</span></dt>
				<dd><p>Project: Clean or Maintain a Swimming Pool</p>
				<p>Comments: Rick did a fantastic job. My pool was stained quite a bit because of me not keeping the chemicals properly adjusted. My pool is 11 years old and it now looks almost as good as new. I don't think any other company can top his work.</p>
				</dd>
			</dl>

			<dl class="testibox">
				<dt>Review by Peter R. in Holiday, <span class="star">&#9733; &#9733; &#9733; &#9733; &#9734;</span></dt>
				<dd><p>Project: Clean or Maintain a Swimming Pool</p>
					<p>Comments: Called them on Friday - holiday weekend and they came right out and examined the green pool. Had chemical in the pool next day. Evaluated the entire pool system - found problems with the motor, impellor, skimmer and filter. Got it all workign so that the realtor could sell the hosue. Thanks</p>
				</dd>
			</dl>

			<dl class="testibox">
				<dt> 06/06/2018 – Annette Guiliani <span class="grade">GRADE A</span></dt>
			<dd><p>Very professional, punctual and does not try to perform unnecessary services</p>
					<p>Category: Pool & Spa Service and Repair</p>
					<p>Services Performed: Yes</p>
				</dd>
			</dl>

			<dl class="testibox">
				<dt> 10/04/2017 – Joan Milloy <span class="grade">GRADE A</span></dt>
			<dd><p>They are doing what I need and that's all I need. They are responsive. After the hurricane and the
			power was out they came and adjusted. They are good.</p>
				<p>Category: Pool & Spa Service and Repair</p>
				<p>Services Performed: Yes</p>
				<p>Cost: $85</p>
				</dd>
			</dl>

			<dl class="testibox">
				<dt> 03/06/2018 – NANCY CAMP <span class="grade">GRADE A</span></dt>
			<dd><p>Now thanks to their hard work, we can swim again in crystal blue water??</p>
				<p>Category: Pool & Spa Service and Repair</p>
				<p>Services Performed: Yes</p>
				</dd>
			</dl>

			<dl class="testibox">
				<dt> 06/19/2017 – Robert Stern <span class="grade">GRADE A</span> </dt>
				<dd><p>It's very hard to find someone that can maintain the pool. It's a very hard job and I am very pleasedwith them. My pool is crystal clear. I pay $85.00 a month. They come on their own and very professional!</p>
				<p>Category: Pool & Spa Service and Repair</p>
					<p>Services Performed: Yes</p>
				</dd>
			</dl>

			<dl class="testibox">
				<dt> 02/19/2018 - Lindsay Stuthers <span class="star">&#9733; &#9733; &#9733; &#9733; &#9733;</span> </dt>
				<dd><p>We have our Pool serviced on a weekly basis and we are very Happy with the Service.Owner is on hand with any questions you might have.</p>
				</dd>
			</dl>
	</div>
</div>
