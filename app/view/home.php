<div id="content">
	<div class="row-m">
		<div class="content-box">
			<div class="left-items">
				<p class="welcome">WELCOME TO</p>
				<h1>All-Pro Pool Service and Repair</h1>
				<p class="info">All Pro Pool & Repair is your 20 year pool and Repair Service professional. With expertise in the hotel business behind us, we are certain we will make sure your pool is serviced and cleaned with the best of care. Call us first, for all of your pool and Repair needs, and you won’t have to look further. Unfortunately we do NOT maintain ABOVE GROUND POOLS. Thank you for your interest!</p>
				<a href="about#content">LEARN MORE</a>
			</div>
			<div class="imgph-right">
				<img src="public/images/content/content-img.png" alt="Content Image 1">
			</div>
		</div>
	</div>
</div>
<div id="services">
	<div class="row">
		<h2>OUR SERVICES</h2>
		<div class="services-box">
			<dl>
				<dt><img src="public/images/content/services-img1.png" alt="Service Image 1"></dt>
				<dd>
					<div class="info">
						<p class="service">Pool Service</p>
						<a href="services#content">LEARN MORE</a>
					</div>
				</dd>
			</dl>
			<dl>
				<dt><img src="public/images/content/services-img2.png" alt="Service Image 2"></dt>
				<dd>
					<div class="info">
						<p class="service">Pool Repairs</p>
						<a href="services#content">LEARN MORE</a>
					</div>
				</dd>
			</dl>
			<dl>
				<dt><img src="public/images/content/services-img3.png" alt="Service Image 3"></dt>
				<dd>
					<div class="info">
						<p class="service">SPECIALS</p>
						<a href="services#content">LEARN MORE</a>
					</div>
				</dd>
			</dl>
		</div>
	</div>
</div>
<div id="info">
	<div class="row">
		<div class="info-circle">
			<p>Be rest assured knowing that your pool is in the best hands by calling us at All-Pro Pool Service and Repair  today.</p>
			<a href="contact#content">GET A FREE QUOTE</a>
		</div>
	</div>
</div>
<div id="testimonials">
	<div class="row-lg">
		<div class="testimonials-box">
			<p class="what">WHAT THEY SAY</p>
			<h2>CLIENT TESTIMONIALS</h2>
			<div class="clients-container">
				<div class="client">
					<p class="name">
						<span>&#9733;&#9733;&#9733;&#9733;&#9733;</span>
						-Cheryl W.
					</p>
					<p class="feedback">
						Rick was fantastic. I called him on a Saturday saying my pool pump wasn’t working and he came out the same day. When he got here it was working and the problem was due to my lack of knowledge. He was wonderful, explained everything to me and gave me some pool tips, He was not going to charge me at all but I also had a pump timer that was not working so I asked him if her could fix that. He had the parts in his truck, fixed/replaced in the middle of a downpour and was very professional and friendly. I am calling him back for another service I need today.
						<span class="bl">I highly recommend.</span>
					</p>
				</div>
				<div class="client">
					<p class="name">
						<span>&#9733;&#9733;&#9733;&#9733;&#9733;</span>
						-Lisa S.
					</p>
					<p class="feedback">
						The pool pump broke and my son called Pool medic they charged me 150.00 and a week later the pump broke again and they refused to fix it. After it broke for the second time My son left it for weeks, by the time I saw the pool it was green, almost blackish and had lots of frogs and smelled so bad I could not stand to walk in his back yard. I called All Pro Pool, he fixed the motor and cleaned up the pool. He came out right away and work was done fast. You had to see the pool, its just unbelievable how he got the pool cleaned up so nice.
						<span class="bl">Would highly recommend this company.</span>
					</p>
				</div>
				<div class="client">
					<p class="name">
						<span>&#9733;&#9733;&#9733;&#9733;&#9733;</span>
						-Peter R.
					</p>
					<p class="feedback">
						Called them on Friday - holiday weekend and they came right out and examined the green pool. Had chemical in the pool next day. Evaluated the entire pool system - found problems with the motor, impellor, skimmer and filter. Got it all workign so that the realtor could sell the hosue. Thanks
					</p>
				</div>
				<div class="client">
					<p class="name">
						<span>&#9733;&#9733;&#9733;&#9733;&#9733;</span>
						-Nanci L.
					</p>
					<p class="feedback">
						Office called me the same day I submitted the request. Came out promptly and assessed the situation. (Stains in bottom of pool) Rick encountered some problems that required extra care. There was staining in the bottom of the pool caused from copper coming off the heater. He was up to the challenge and did a great job.
					</p>
				</div>
				<a href="testimonials#content" class="test-btn">READ MORE</a>
			</div>
		</div>
	</div>
</div>
<div id="gallery">
	<div class="row">
		<h2>RECENT WORKS</h2>
		<h1>OUR GALLERY</h1>
		<div class="gallery-images">
			<img src="public/images/content/gallery1.jpg" alt="Outdoor Pool" class="inbTop">
			<img src="public/images/content/gallery2.jpg" alt="Pool Tools" class="inbTop">
			<img src="public/images/content/gallery3.jpg" alt="Pool Net" class="inbTop">
			<img src="public/images/content/gallery4.jpg" alt="Indoor Pool " class="inbTop">
		</div>
		<a href="<?php echo URL ?>gallery#content" class="gal-btn">View Gallery</a>
	</div>
</div>
<div id="contact-details">
	<div class="row">
		<div class="contactTop">
			<div class="left col-6 inbTop">
				<h1>We Proudly Offer:</h1>
				<ul>
					<li> <p>WEEKLY FULL SERVICE: Maintain chemicals, Brush tiles, walls & steps, Clean pump & skimmer baskets, Net, Inspect equipment, Vacuum and clean filter as needed: CAGED pools start @ $85 and OPEN pools start @ $95. Spa Xtra.</p> </li>
					<li> <p>Green Pool Clean-Up with chemicals starting at $189 or Drain, acid wash, refill and balance chemicals. Most pools $500.</p> </li>
					<li> <p>ind out about the benefits of a salt pool.</p> </li>
					<li> <p>Motor/Pumps, Filter and Timer Repair or Replace<br> (New Pool Motors Starting at $350 installed)</p> </li>
					<li> <p>General Pool Plumbing Repairs<br> Residential<br> Commercial</p> </li>
				</ul>
			</div>
			<div class="right col-6 inbTop">
				<div class="contact-panel">
					<h1>LET US KNOW WHAT YOU NEED <span>CONTACT US</span></h1>
					<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
						<input type="text" name="name" placeholder="Name:">
						<input type="text" name="email" placeholder="Email:">
						<input type="text" name="phone" placeholder="Phone:">
						<p>Required: Please provide your address.</p>
						<textarea name="message" cols="30" rows="10" placeholder="Message:"></textarea>
						<div class="g-recaptcha"></div>
						<p><input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.</p><br>
						<?php if( $this->siteInfo['policy_link'] ): ?>
						<p><input type="checkbox" name="termsConditions" class="termsBox"/>I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a></p>
						<?php endif ?>
						<button type="submit" class="ctcBtn btn">SUBMIT FORM</button>
					</form>
				</div>
				<div class="donations">
					<img src="public/images/common/card1.png" alt="Donate 1" class="inbTop">
					<img src="public/images/common/card2.png" alt="Donate 2" class="inbTop">
					<img src="public/images/common/card3.png" alt="Donate 3" class="inbTop">
					<img src="public/images/common/card4.png" alt="Donate 4" class="inbTop">
					<img src="public/images/common/card5.png" alt="Donate 5" class="inbTop">
					<img src="public/images/common/card6.png" alt="Donate 6" class="inbTop">
				</div>
			</div>
		</div>
		<div class="contactBot">
			<div class="left col-4 inbMid">
				<a href="home"> <img src="public/images/common/mainLogo.png" alt="All-Pro Pool Service and Repair Logo"/> </a>
			</div>
			<div class="mid col-3 inbMid">
				<img src="public/images/common/phone.png" alt="Phone">
				<p class="phone"><?php $this->info(["phone","tel"]); ?></p>
				<p>
					<a href="<?php $this->info("fb_link"); ?>" class="socialMedia">f</a>
					<a href="<?php $this->info("gp_link"); ?>" class="socialMedia">g</a>
					<a href="<?php $this->info("li_link"); ?>" class="socialMedia">i</a>
					<a href="<?php $this->info("tt_link"); ?>" class="socialMedia">l</a>
					<a href="<?php $this->info("yt_link"); ?>" class="socialMedia">x</a>
				</p>
			</div>
			<div class="right col-2 inbMid">
				<img src="public/images/common/email.png" alt="Email">
				<p class="email"><?php $this->info(["email","mailto"]); ?></p>
				<img src="public/images/common/address.png" alt="Addess">
				<p class="address"><?php $this->info("address"); ?></p>
			</div>
		</div>
	</div>
</div>
