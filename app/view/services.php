<div id="content">
	<div class="row">
		<h1>Services</h1>
		<div class="inner-services">
			<div id="repairs" class="svc-box">
				<h2>REPAIRS <span>in-ground pools and spas only</span> </h2>
				<p>Our expert repair tech will get you pool up and running again and you know the job is done professionally and correctly. To save yourself time and money first troubleshoot. Make sure your filter is clean and all of your o-rings are lubed and in place, empty your skimmer basket and pump basket. If your motor is not running,check all of your breakers, inside and out. If your motor is making any strange noises turn it off and call. Sometimes debris is caught in the impeller and just needs cleaning. This can save a motor and a couple hundred dollars. In most cases when the pump stops it is just the motor that needs replacing. If your motor is under heavy water keep a cover on it. ALWAYS make sure you maintain the proper water level, midway of your skimmer.</p>
				<p>Upgrading your pool to a salt water pool will add years of life to your finish and equipment. It will also help maintain your skin, hair leaving it feeling soft, and those expensive bathing suits will last much longer. Salt generators continually maintain a safe constant level of sanitizer. This avoids the initial high levels of chlorine added to maintain sanitary conditions throughout the week. A salt pool will also cut down your maintenance costs and time. It is a good idea to have an auto chlorinator filled with tabs in the winter as the pool need not run more than 4 hours per day.</p>
				<p>If you have a cartridge filter a new cartridge is needed every 1 to 2 years depending on bathing level. A DE filter requires the grids to be removed and cleaned 1-2 times per year, again. depending on bathing level. If you see powder coming back through the returns it is time to replace the grids and or manifold. A sand filter should have the sand replaced every 3-5 years.</p>
				<ul>
					<li><p>Motor replacement starting at $350 installed</p></li>
					<li><p>pumps for sale and replaced or repaire</p></li>
					<li><p>Filters for sale and installed or repaired</p></li>
					<li><p>Salt systems for sale and installed or repaired</p></li>
					<li><p>Timers installed</p></li>
					<li><p>Auto tab chlorinators for sale and installed or repaired</p></li>
					<li><p>Lines unclogged</p></li>
					<li><p>Above ground plumbing repairs of leaks or relocate</p></li>
					<li><p>Auto cleaner sales and repair</p></li>
					<li><p>Automation systems for sale and installed</p></li>
					<li><p>Blowers installed</p></li>
					<li><p>Heat pumps for sale and installed (never gas)</p></li>
					<li><p>Pool lights sold and installed or repaired ASK ABOUT OUR COLOR CHANGING LED BULBS for pools and spas</p></li>
					<li><p>Hand rails and ladders sold and installed</p></li>
					<li><p>Seal and o-ring replacement</p></li>
					<li><p>Filter elements, baskets, o-rings for sale and install</p></li>
					<li><p>DE grids cleaned or replaced</p></li>
					<li><p>Sand filters cleaned and sand replaced with Zeo Sand</p></li>
					<li><p>Salt Cell Cleaning</p></li>
					<li><p>Acid Washing Filter</p></li>
					<li><p>No flow problems addressed.</p></li>
				</ul>
			</div>
			<div id="green-pools" class="svc-box">
				<h2>GREEN POOLS</h2>
				<p>Green to clean starting at $189 (chemically turn your pool from green to clear blue) Drain and acid wash concrete pools and spas starting at $495 (small to medium pools max 6' deep large and deep pools extra) drain, power wash and acid wash, fill with client's water and balance chemicals.</p>
				<p>Drain and chlorine wash fiberglass pools- drain power wash and chlorine wash, fill with customer's water, balance chemicals. Starting at $450</p>
			</div>
			<div id="pool-service" class="svc-box">
				<h2>POOL SERVICE</h2>
				<p>Having a pool service not only saves you time but can actually save you money. Keeping your pool water balanced to the proper PH, alkalinity and calcium hardness levels is a must in keeping your pool, you, and your family healthy. Your pool is for enjoyment not to add extra work after a long day or week. Our service team will ensure that your pool is maintained to the upmost quality.</p>
				<p>Weekly full service- caged pools start @ $85 / month includes basic chemicals, brushing of tiles and walls, netting the top, vacuum up to 2x per month, clean filter as needed, emptying baskets and inspecting pool equipment. Open pools start @ $95 which includes the above but will vac as needed.</p>
				<p>Chemical only service includes maintaining basic chemicals weekly and cleaning the filter monthly starting @ $65. Client must brush walls weekly.</p>
				<p>Bi-weekly chemical only SALT POOLS ONLY starting @ $50. Client must brush walls, net and vacuum</p>
				<p>Bi-weekly full service SALT POOLS ONLY starting @ $75 </p>
			</div>
		</div>
	</div>
</div>
